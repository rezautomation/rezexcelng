package com.rezgateway.automation.input;

import java.util.ArrayList;
import java.util.HashMap;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExcelReaderTest {

	private ExcelReader excel = null;
	private static String xlspath1 = "resources/HotelScenarios.xls";
	private static String xlspath2 = "resources/textexcel1.xls";
	private static String xlsxpath = "resources/textexcel1.xlsx";

	// private static String invpath = "C:/Users/Dulan/rezexcelng/TestResources/textexcel1.xl";

	@Before
	public void setUp() {
		excel = new ExcelReader();
	}

	@Test
	public void testWithXls() throws Exception {
		// excel = new ExcelReader();
		String test = excel.getSheetData(xlspath2, 0).getRow(0).getCell(0).getStringCellValue();
		Assert.assertEquals("Test1", test);
	}

	@Test
	public void testWithGetExcelData() throws Exception {
		String[][] test = excel.getExcelData(xlspath1, "Reservation");
		Assert.assertNotNull(test);
		//Assert.assertEquals("Test1", Arrays.toString(test));
	}

	@Test
	public void testwithXlsx() throws Exception {
		// excel = new ExcelReader();
		String test = excel.getSheetData(xlsxpath, 0).getRow(0).getCell(0).getStringCellValue();
		Assert.assertEquals("Test1", test);
	}

	@Test
	public void testForAllSheetsXls() throws Exception {
		// excel = new ExcelReader();
		ArrayList<org.apache.poi.ss.usermodel.Sheet> sheetlist = excel.getAllSheetData(xlspath2);
		Assert.assertEquals(3, sheetlist.size());
	}

	@Test
	public void testForAllSheetsWithNamesXls() throws Exception {
		// excel = new ExcelReader();
		HashMap<String, org.apache.poi.ss.usermodel.Sheet> sheetlist = excel.getAllSheetDataWithTitel(xlspath2);
		Assert.assertEquals("Test1", sheetlist.get("FirstSheet").getRow(0).getCell(0).getStringCellValue());
	}

	@Test
	public void testForAllSheetsXlsx() throws Exception {
		// excel = new ExcelReader();
		ArrayList<org.apache.poi.ss.usermodel.Sheet> sheetlist = excel.getAllSheetData(xlsxpath);
		Assert.assertEquals(3, sheetlist.size());
	}

	@Test
	public void testForAllSheetsWithNamesXlsx() throws Exception {
		// excel = new ExcelReader();
		HashMap<String, org.apache.poi.ss.usermodel.Sheet> sheetlist = excel.getAllSheetDataWithTitel(xlsxpath);
		Assert.assertEquals("Test1", sheetlist.get("FirstSheet").getRow(0).getCell(0).getStringCellValue());
	}

	@Test
	public void testForInvalidExtention() throws Exception {

	}

	@Test
	public void testForFilenotFound() {

	}

	@After
	public void tearDown() {

	}

}
