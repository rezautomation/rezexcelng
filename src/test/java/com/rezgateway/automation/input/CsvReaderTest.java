package com.rezgateway.automation.input;

import java.io.IOException;

import org.apache.commons.csv.CSVRecord;
import org.junit.Test;

public class CsvReaderTest {
	
	String filePath = "resources/RateExport_190217_1903V4.csv"; // with header
	String filePath1 = "resources/tariffdailyreport_190116_1_V2.csv"; //without header
	boolean isHeader = true;
	
	@Test
	public void readCsvWithHeader() throws IOException {
		CsvReader reader = new CsvReader();
		int count = 0;
		
		if(isHeader == true){
			for (CSVRecord record : reader.csvRead(filePath, isHeader)) {				
				if (record.get("hotelCode").equals("5714")) {
					String satrtDate = record.get("StartDate");
					String endDate = record.get("EndDate");					
					String hotelCode = record.get("hotelCode");
					String dailyRates = record.get("dailyRatesAfterPromotion");
					String totalRate = record.get("totalRateAfterPromotion");
					
					System.out.println("Start : " + satrtDate+ "\tEnd : " + endDate + "\tHotelCode : " + hotelCode + "\tTotal rate : " + totalRate + "\tDailyRates : " + dailyRates);
					count++;
				}
			}
			System.out.println("Total count : " + count);
		}else{
			for (CSVRecord record : reader.csvRead(filePath1, isHeader)) {
				if (record.get(1).equals("5714")) { 
					String satrtDate = record.get(9);
					String hotelName = record.get(0);
					String hotelCode = record.get(1);
					String totalRate = record.get(11);
					
					System.out.println("Start : " + satrtDate + "\tHotelCode : " + hotelCode + "\tHotelName : " + hotelName + "\tTotal rate : " + totalRate);
					count++;
				}
			}
			System.out.println("Total count : " + count);
		}
	}


}
