package com.rezgateway.automation.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

public class CsvReader {
		
	public CSVParser csvRead(String filePath, boolean withHeader) throws IOException{
		BufferedReader br = null;
		try {
			FileReader reader = new FileReader(filePath);
			br = new BufferedReader(reader);
		} catch (Exception e) {
			System.out.println("Exception : " + e.getMessage());
		}
		
		CSVParser parser = null;
		if (withHeader == true) {
			parser = new CSVParser(br,CSVFormat.DEFAULT.withFirstRecordAsHeader().withTrim());
		}else{
			parser = new CSVParser(br, CSVFormat.DEFAULT.withTrim());
		}	
		
		return parser;
	}	
}
